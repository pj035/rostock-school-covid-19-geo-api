/* eslint no-console: 0 */ // --> OFF
// NOTE/impl: disable logging rule for debugging purpose on vercel.com

import { NowRequest, NowRequestQuery, NowResponse } from '@vercel/node';

import { IAPIResponse } from '../../api-models';
import { getDataForCounty } from '../../rki';
import { reverse } from '../../nominatim';
import {
  EntityNotFoundError,
  getErrorPayload,
  InvalidDataError,
} from '../../errors';
import { enableCors } from '../../api-helper';
import { BERLIN_STATES_MAP, getCountyName } from '../../helper';

// TODO/impl: write tests
function parseQueryParams(query: NowRequestQuery) {
  const { lat, lng } = query;

  if (!lat || !lng) {
    throw new InvalidDataError('lat or lng query parameter missing');
  }

  if (Array.isArray(lat) || Array.isArray(lng)) {
    throw new InvalidDataError('lat or lng query parameter incorrect format');
  }

  let iLat: number, iLng: number;
  try {
    iLat = parseFloat(lat);
    iLng = parseFloat(lng);
  } catch (e) {
    throw new InvalidDataError('lat or lng query parameter incorrect format');
  }

  return {
    lat: iLat,
    lng: iLng,
  };
}

const handler = async (req: NowRequest, res: NowResponse) => {
  try {
    const { lat, lng } = parseQueryParams(req.query);
    // NOTE/impl: fetch position
    const position = await reverse(lat, lng);
    console.log(
      `Fetched position ${JSON.stringify(position)} at ${lat} | ${lng}`,
    );
    const address = position.address;
    if (!address) {
      throw new EntityNotFoundError('Position could not be identified.');
    }
    // TODO/impl: research/find docs about the reverse lookup response
    const county = address.county || '';
    const city = address.city || '';
    // NOTE/impl: Currently the logic of Nominatim usage assumes that
    // if county is not defined we are in a city, which leads to a defined city property.
    let countyName = '',
      countyType = '';
    if (county) {
      // NOTE/impl: currently assuming that "Landkreis" is a prefx
      countyName = county.replace('Landkreis ', '');
      countyType = 'LK';
    } else if (city) {
      countyName = city;
      countyType = 'SK';
    } else {
      // NOTE/impl: special cases like Berlin
      if (address.state && address.state === 'Berlin') {
        const tmpName = getCountyName(address, BERLIN_STATES_MAP);
        countyName = tmpName ? `Berlin ${tmpName}` : '';
        countyType = 'SK';
      }
    }

    if (!countyName || !countyType) {
      throw new EntityNotFoundError('Position could not be identified.');
    }

    console.log(`Fetch data for ${countyType} ${countyName}`);
    const data = await getDataForCounty(countyName, countyType as 'LK' | 'SK');
    const attrs = data.attributes;
    console.log(
      `Got RKI data for ${countyType} ${countyName}: ${JSON.stringify(attrs)}`,
    );
    const retData: IAPIResponse = {
      timestamp: attrs.last_update,
      county: {
        name: attrs.county,
        cases: attrs.cases,
        deaths: attrs.deaths,
        casesPer100KLast7Days: attrs.cases7_per_100k,
        inhabitants: attrs.EWZ,
      },
      state: {
        name: attrs.BL,
        inhabitants: attrs.EWZ_BL,
        casesPer100KLast7Days: attrs.cases7_bl_per_100k,
      },
    };

    res.setHeader('Cache-Control', 's-maxage=3600');
    return res.status(200).json({ data: retData });
  } catch (e) {
    const resPayload = getErrorPayload(e);
    return res.status(resPayload.code).json(resPayload.payload);
  }
};

export default enableCors(handler);
