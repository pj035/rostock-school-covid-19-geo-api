import { getCountyName } from './extract-county-names';
import { BERLIN_STATES_MAP } from './berlin-state.map';
import { IReverseResponse } from '../nominatim';

type Address = IReverseResponse['address'];

describe('extract-county-name.ts', () => {
  it('should return null', () => {
    const addr1: Address = {};
    const addr2: Address = {
      suburb: 'HafenCity',
    };
    const addr3: Address = {
      borough: 'HafenCity,',
    };

    for (const addr of [addr1, addr2, addr3]) {
      const result = getCountyName(addr, BERLIN_STATES_MAP);
      expect(result).toBeNull();
    }
  });

  it('should return the correctly extracted value', () => {
    const addr1: Address = {
      suburb: 'Spandau',
    };
    const addr2: Address = {
      suburb: 'Staaken',
      borough: 'Spandau',
    };

    for (const addr of [addr1, addr2]) {
      const result = getCountyName(addr, BERLIN_STATES_MAP);
      expect(result).toBe('Spandau');
    }
  });
});
