import { IStateMap } from './berlin-state.map';
import { IReverseResponse } from '../nominatim';

/**
 * Extract the county name from more complex and bigger states like Berlin. Requires a static state map of such area
 * that is derived from Nominatim results.
 *
 * @param address
 * @param stateMap
 */
export function getCountyName(
  address: IReverseResponse['address'],
  stateMap: IStateMap,
) {
  for (const name of stateMap.stateNames) {
    for (const key of stateMap.nominatimKeys) {
      // TODO/impl. types
      if (key in address && (address as any)[key] === name) {
        return name;
      }
    }
  }

  return null;
}
