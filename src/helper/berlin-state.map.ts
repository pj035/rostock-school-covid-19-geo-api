export interface IStateMap {
  /** Keys in nominatim reverse response to validate against. */
  nominatimKeys: string[];
  /** Valid state names to extract. */
  stateNames: string[];
}

export const BERLIN_STATES_MAP: IStateMap = {
  nominatimKeys: ['suburb', 'borough'],
  stateNames: [
    'Charlottenburg-Wilmersdorf',
    'Friedrichshain-Kreuzberg',
    'Lichtenberg',
    'Marzahn-Hellersdorf',
    'Mitte',
    'Neukölln',
    'Pankow',
    'Reinickendorf',
    'Spandau',
    'Steglitz-Zehlendorf',
    'Tempelhof-Schöneberg',
    'Treptow-Köpenick',
  ],
};
