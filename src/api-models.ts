export interface IAPIResponse {
  timestamp: string;
  county: {
    name: string;
    cases: number;
    deaths: number;
    casesPer100KLast7Days: number;
    inhabitants: number;
  };
  state: {
    name: string;
    casesPer100KLast7Days: number;
    inhabitants: number;
  };
}
