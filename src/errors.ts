// TODO: document

class AppError extends Error {
  private _code: EAPIError;

  constructor(code: EAPIError, msg: string) {
    super(msg);
    this._code = code;
  }

  get code() {
    return this._code;
  }
}

export enum EAPIError {
  ENTITY_NOT_FOUND = 'EENTITY_NOT_FOUND',
  INVALID_DATA = 'EINVALID_DATA',
}

export class InvalidDataError extends AppError {
  constructor(msg: string) {
    super(EAPIError.INVALID_DATA, msg);
  }
}

export class EntityNotFoundError extends AppError {
  constructor(msg: string | null) {
    super(EAPIError.INVALID_DATA, msg ? msg : 'Entity not found');
  }
}

export function getErrorPayload(error: AppError | Error) {
  const unknownError = {
    payload: {
      error: {
        name: 'EUNKNOWN_ERROR',
        message: 'Unexpected error occured.',
      },
    },
    code: 500,
  };
  // NOTE/impl: generic error
  if (!(error instanceof AppError)) {
    /*  eslint-disable-next-line */
    console.error('Unknown error', error);
    return unknownError;
  }

  switch ((error as AppError).code) {
    case EAPIError.INVALID_DATA:
      return {
        payload: {
          error: {
            name: EAPIError.INVALID_DATA,
            message: error.message,
          },
        },
        code: 406,
      };
    case EAPIError.ENTITY_NOT_FOUND:
      return {
        payload: {
          error: {
            name: EAPIError.ENTITY_NOT_FOUND,
            message: error.message,
          },
        },
        code: 404,
      };
    default:
      return unknownError;
  }
}
