import axios from 'axios';
import { stringify } from 'querystring';

import { IReverseResponse } from './model';

const baseUrl = 'https://nominatim.openstreetmap.org';

/**
 * Calls `/reverse` endpoint of Nominatim service.
 *
 * @url https://nominatim.org/release-docs/develop/api/Reverse/
 */
export async function reverse(lat: number, lng: number) {
  const queryOpts = {
    format: 'json',
    lat,
    lon: lng,
    addressdetails: 1,
    zoom: 14,
  };
  const url = `${baseUrl}/reverse?${stringify(queryOpts)}`;

  const response = await axios.get<IReverseResponse>(url);

  return response.data;
}
