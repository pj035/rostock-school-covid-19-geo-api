import axios from 'axios';

import { reverse } from './api';
import { IReverseResponse } from './model';

describe('api.ts', () => {
  describe('reverse', () => {
    it('should call reverse with provided lat and lng values', async () => {
      const lat = 22;
      const lng = 11;

      const mockResponse: IReverseResponse = {
        lat: String(lat),
        lon: String(lng),
        display_name: 'Mocked Address',
        place_id: 1,
        osm_type: 'Type',
        license: 'License',
        address: {},
      };

      const spy = jest.spyOn(axios, 'get').mockImplementation(() =>
        Promise.resolve({
          data: mockResponse,
        }),
      );

      await reverse(lat, lng);

      const expectedUrl = `https://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${lng}&addressdetails=1&zoom=14`;

      expect(spy).toBeCalledWith(expectedUrl);
      expect(spy).toBeCalledTimes(1);
    });
  });
});
