// TODO/impl: set optional / nullable properties
/**
 * @url https://nominatim.org/release-docs/develop/api/Reverse/
 * @url https://nominatim.org/release-docs/develop/api/Output/#addressdetails
 */
export interface IReverseResponse {
  place_id: number;
  license: string;
  osm_type: string;
  lat: string;
  lon: string;
  display_name: string;
  address: {
    village?: string;
    municipality?: string;
    suburb?: string;
    city?: string;
    borough?: string;
    county?: string;
    state?: string;
    postcode?: string;
    country?: string;
    country_code?: string;
  };
}
