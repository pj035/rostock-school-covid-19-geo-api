import axios from 'axios';

import { getDataForCounty } from './api';
import RKILandkreisMock from './mocks/rki_landkreis-response.mock.json';

describe('api.ts', () => {
  describe('getDataForCounty', () => {
    const county = 'Rostock';
    const type = 'LK';
    it('should call the API with provided county and countyType', async () => {
      const expectedUrl =
        'https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_Landkreisdaten/FeatureServer/0/query?' +
        `where=county%20%3D%20'${type.toUpperCase()}%20${county.toUpperCase()}'&` +
        'outFields=GEN,BEZ,BEM,EWZ,KFL,death_rate,cases,deaths,cases_per_100k,cases_per_population,BL,BL_ID,county,last_update,cases7_per_100k,recovered,EWZ_BL,cases7_bl_per_100k&'.replace(
          /,/g,
          '%2C',
        ) +
        'returnGeometry=false&' +
        'outSR=4326&' +
        'f=json';

      const spy = jest.spyOn(axios, 'get').mockImplementation(() =>
        Promise.resolve({
          data: RKILandkreisMock,
        }),
      );

      await getDataForCounty(county, type);

      expect(spy).toBeCalledWith(expectedUrl);
      expect(spy).toBeCalledTimes(1);
    });

    it('should return the first feature set', async () => {
      const spy = jest.spyOn(axios, 'get').mockImplementation(() =>
        Promise.resolve({
          data: RKILandkreisMock,
        }),
      );

      const data = await getDataForCounty(county, type);

      expect(spy).toBeCalledTimes(1);
      expect(data).toEqual(RKILandkreisMock.features[0]);
    });

    it('should throw an error if no feature set is defined', async () => {
      const spy = jest.spyOn(axios, 'get').mockImplementation(() =>
        Promise.resolve({
          data: Object.assign({}, RKILandkreisMock, { features: [] }),
        }),
      );

      await expect(getDataForCounty(county, type)).rejects.toThrowError(
        'Empty FeatureSet',
      );
      expect(spy).toBeCalledTimes(1);
    });
  });
});
