/**
 * @url https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/917fc37a709542548cc3be077a786c17_0/geoservice
 */
export interface IRKILandkreisResponse {
  features: IFeature[];
}

/**
 * Feature field per Landkreis.
 */
interface IFeature {
  attributes: {
    /** Name of area */
    GEN: string;
    /** Type of area like county or city. */
    BEZ: string;
    BEM: string;
    /** Number of inhabitants. */
    EWZ: number;
    /** Inhabitants of whole state (Bundesland). */
    EWZ_BL: number;
    /** Ratio between cases and deaths. */
    death_rate: number;
    /** Number of reported cases. */
    cases: number;
    /** Number of reported deaths. */
    deaths: number;
    /** Numkber of cases calculated per 100k inhabitants. */
    cases_per_100k: number;
    /** State / Bundesland */
    BL: string;
    /* Unique ID of BL */
    BL_ID: string;
    /** The county.
     * Usually prefixed with LK or SK dependent of BEZ (LK for Landkreis; SK for cities).
     */
    county: string;
    /**
     * Update time stamp.
     * Format: `dd.mm.yyyy, hh:mm Uhr`
     */
    last_update: string;
    /** Cases per 100k inhabitants of last 7 days. */
    cases7_per_100k: number;
    /** Cases per 100k inhabitants of whole state (Bundesland) of last 7 days. */
    cases7_bl_per_100k: number;
    /** Ratio of cases and inhabitants. */
    cases_per_population: number;
  };
}
