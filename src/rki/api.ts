import axios from 'axios';
import { stringify } from 'querystring';

import { IRKILandkreisResponse } from './models';

// TODO/impl: refactor `countyType`
/**
 * Returns Covid19 data for provided county.
 * @param county name/id of county
 * @param countyType type of the county
 */
export async function getDataForCounty(
  county: string,
  countyType: 'LK' | 'SK',
) {
  const base =
    'https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_Landkreisdaten/FeatureServer/0/query';
  const queryParams = {
    // where: `county%20%3D%20%27${countyType.toUpperCase()}%20${county.toUpperCase()}%27`,
    where: `county = '${countyType.toUpperCase()} ${county.toUpperCase()}'`,
    outFields:
      'GEN,BEZ,BEM,EWZ,KFL,death_rate,cases,deaths,cases_per_100k,cases_per_population,BL,BL_ID,county,last_update,cases7_per_100k,recovered,EWZ_BL,cases7_bl_per_100k',
    returnGeometry: false,
    outSR: 4326,
    f: 'json',
  };
  const url = `${base}?${stringify(queryParams)}`;

  const response = await axios.get<IRKILandkreisResponse>(url);
  const data = response.data;
  if (data.features && data.features.length) {
    // TODO/impl: handle case of multiple features
    return data.features[0];
  } else {
    throw new Error('Empty FeatureSet');
  }
}
