# Rostock School Covid-19 GEO API

Simple API that takes lat and long values and requests Covid-19 Data for Germany from RKI.


## Sources

* OpenstreetMap Nominatim https://nominatim.org/release-docs/develop/api/Reverse/
* RKI Data for Germany https://npgeo-corona-npgeo-de.hub.arcgis.com/search?groupIds=b28109b18022405bb965c602b13e1bbc

## Vercel Deployment

1. Register on https://vercel.com/
2. Link your git repo to your vercel account
  * click on the project > settings > git
3. Set root directory under settings > general to `src`
4. On your command line run `vercel --prod` to verify that everything works fine
  * set the scope to your username
  * link the local project to your remote project on vercel.com

Note, this guide is not complete and likely lacks a few things. If you don't have a vercel project in your dashboard you likely have to run `vercel` to initialize everything.

## API Docs

*Very simplified.*

* Base Path of API is `https://covid-api-rki.vercel.app/api`
* Version: `v1`


### Endpoints

* `GET /county-data-by-position`
* Query params
   * `lat` (*required*): latitude value
   * `lng` (*required*): longitude value

**Example**

`GET https://covid-api-rki.vercel.app/api/v1/county-data-by-position?lat=54.083333&lng=12.133333`

Response

```json
{
    "data": {
        "timestamp": "22.11.2020, 00:00 Uhr",
        "county": {
            "name": "SK Rostock",
            "cases": 589,
            "deaths": 6,
            "casesPer100KLast7Days": 36.8084669034519,
            "inhabitants": 209191
        },
        "state": {
            "name": "Mecklenburg-Vorpommern",
            "inhabitants": 1608138,
            "casesPer100KLast7Days": 46.3268699576778
        }
    }
} 
```